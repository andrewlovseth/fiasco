<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		
	</section>


	<section id="main">
			
		<section id="photos">

			<div class="gallery">

				<div class="col-1">
					<div class="col-2">
						<img src="<?php $image = get_field('top_left_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="col-2">
						<img src="<?php $image = get_field('top_right_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

						<img src="<?php $image = get_field('middle_right_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

					</div>						
				</div>

				<div class="col-1 wide">
					<img src="<?php $image = get_field('bottom_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			</div>
			
		</section>

		<section id="info">
			
			<div class="description">
				<h3><?php the_field('headline'); ?></h3>
				<div class="copy">
					<?php the_field('description'); ?>
				</div>
			</div>

			<div class="contact-details">

				<div class="hours details">
					<h3>Hours</h3>
					<p><?php the_field('hours', 'options'); ?></p>
				</div>

				<div class="location details">
					<h3>Location</h3>
					<p><?php the_field('location', 'options'); ?></p>
				</div>
				
			</div>




		</section>

		<?php get_template_part('partials/next-door-teaser'); ?>

	</section>


<?php get_footer(); ?>